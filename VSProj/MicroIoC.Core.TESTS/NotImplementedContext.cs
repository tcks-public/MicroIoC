﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MicroIoC.Core.TESTS
{
	public class NotImplementedContext : IContext {
		public IContainer Factories => throw new NotImplementedException();

		public IContainer Providers => throw new NotImplementedException();

		public IContext CreateMainContext() => throw new NotImplementedException();
		public IContext CreateDerivedContext(IContext parentContext) => throw new NotImplementedException();
	}
}

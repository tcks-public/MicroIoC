﻿using System;
using System.Collections.Generic;
using System.Text;
using MicroIoC.Impl;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MicroIoC.Core.TESTS.Impl
{
	[TestClass]
	public class ConcurrentRoot_Tests {
		[TestMethod]
		public void ResetWorks() {
			var subject = new ConcurrentRoot();
			Assert.IsNull(subject.LocalContext);
			Assert.IsNotNull(subject.MainContext);
			Assert.IsNull(subject.LocalContext);

			{
				var prevMainContext = subject.MainContext;
				subject.ResetMainContext();
				Assert.IsNull(subject.LocalContext);
				var currMainContext = subject.MainContext;
				Assert.IsNotNull(currMainContext);
				Assert.AreNotSame(prevMainContext, currMainContext);
				Assert.IsNull(subject.LocalContext);
			}

			var otherContext = new NotImplementedContext();
			subject.ResetMainContext(otherContext);
			Assert.AreSame(otherContext, subject.MainContext);
			Assert.IsNull(subject.LocalContext);
			Assert.AreSame(otherContext, subject.MainContext);
			Assert.IsNull(subject.LocalContext);

			subject.ResetMainContext();

			Assert.IsNotNull(subject.MainContext);
			Assert.IsNull(subject.LocalContext);
		}

		[TestMethod]
		public void RunInContextWorksInSingleThread() {
			var subject = new ConcurrentRoot();

			Assert.IsNull(subject.LocalContext);

			var context01 = new NotImplementedContext();
			subject.RunInContext(context01, () => {
				Assert.AreSame(context01, subject.LocalContext);

				var context02 = new NotImplementedContext();
				subject.RunInContext(context02, () => {
					Assert.AreSame(context02, subject.LocalContext);
				});

				Assert.AreSame(context01, subject.LocalContext);
			});

			Assert.IsNull(subject.LocalContext);
		}

		[TestMethod]
		public void RunInDerivedContextWorksInSingleThread() {
			var subject = new ConcurrentRoot();

			var mainContext = subject.MainContext;
			Assert.IsNotNull(mainContext);
			Assert.IsNull(subject.LocalContext);

			var context01 = new ConcurrentContext();
			subject.RunInDerivedContext(context01, () => {
				var currContext01 = (IDerivedContext)subject.LocalContext;
				Assert.AreSame(context01, currContext01.ChildContext);
				Assert.AreSame(mainContext, currContext01.ParentContext);

				var context02 = new ConcurrentContext();
				subject.RunInDerivedContext(context02, () => {
					var currContext02 = (IDerivedContext)subject.LocalContext;
					Assert.AreSame(context02, currContext02.ChildContext);
					Assert.AreSame(currContext01, currContext02.ParentContext);

					subject.RunInDerivedContext(() => {
						var currContext03 = (IDerivedContext)subject.LocalContext;
						Assert.IsNotNull(currContext03.ChildContext);
						Assert.AreNotSame(currContext02, currContext03.ChildContext);
						Assert.AreNotSame(currContext01, currContext03.ChildContext);
						Assert.AreNotSame(mainContext, currContext03.ChildContext);

						Assert.AreSame(currContext02, currContext03.ParentContext);
					});
				});
			});
		}
	}
}

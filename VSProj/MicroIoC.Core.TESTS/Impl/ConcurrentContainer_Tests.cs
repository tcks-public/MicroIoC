﻿using System;
using System.Collections.Generic;
using System.Text;
using MicroIoC.Impl;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MicroIoC.Core.TESTS.Impl
{
	[TestClass]
	public class ConcurrentContainer_Tests {
		[TestMethod]
		public void WorksWhenIsEmpty() {
			var subject = new ConcurrentContainer();

			{
				var factory01 = subject.Peek<object>();
				Assert.IsNull(factory01);

				Assert.ThrowsException<MissingContainerItemException>(() => subject.Get<object>());
			}

			{
				var factory01 = subject.Peek(typeof(object));
				Assert.IsNull(factory01);

				Assert.ThrowsException<MissingContainerItemException>(() => subject.Get(typeof(object)));
			}

			{
				var factory02 = subject.Peek<int>();
				Assert.IsNull(factory02);

				Assert.ThrowsException<MissingContainerItemException>(() => subject.Get<int>());
			}

			{
				var factory02 = subject.Peek(typeof(int));
				Assert.IsNull(factory02);

				Assert.ThrowsException<MissingContainerItemException>(() => subject.Get(typeof(int)));
			}

			{
				var factory03 = subject.Peek<object[]>();
				Assert.IsNull(factory03);

				Assert.ThrowsException<MissingContainerItemException>(() => subject.Get<object[]>());
			}

			{
				var factory03 = subject.Peek(typeof(object[]));
				Assert.IsNull(factory03);

				Assert.ThrowsException<MissingContainerItemException>(() => subject.Get(typeof(object[])));
			}
		}
	}
}

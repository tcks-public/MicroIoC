﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace MicroIoC {
	public class MissingContextException : Exception {
		public MissingContextException() : base("The IoC context is missing.") { }

		public MissingContextException(string message) : base(message) { }

		public MissingContextException(string message, Exception innerException) : base(message, innerException) { }

		protected MissingContextException(SerializationInfo info, StreamingContext context) : base(info, context) { }
	}
}

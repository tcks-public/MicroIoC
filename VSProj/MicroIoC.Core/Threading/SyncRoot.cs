﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MicroIoC.Threading {
	public sealed partial class SyncRoot {
		public class Locker { }

		private readonly Locker root = new Locker();
	}

	partial class SyncRoot {
		public T DoubleCheckedNotNull<T>(ref T field, Func<T> factory) where T : class {
			var result = field;
			if (!(result is null)) {
				return result;
			}

			lock (root) {
				result = field;
				if (!(result is null)) {
					return result;
				}

				result = factory();
				if (result is null) {
					throw new InvalidOperationException("Result of factory method is null.");
				}

				field = result;
				return result;
			}
		}
	}
}

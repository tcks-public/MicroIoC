﻿using System;
using System.Collections.Generic;
using System.Text;
using MicroIoC.Core;

namespace MicroIoC.Impl {
	public sealed class ConcurrentContext : ConcurrentContextBase {
		private readonly ConcurrentContainer factories = new ConcurrentContainer();
		public ConcurrentContainer Factories => this.factories;
		protected override IContainer IContext_Factories => this.Factories;

		private readonly ConcurrentContainer providers = new ConcurrentContainer();
		public ConcurrentContainer Providers => this.providers;
		protected override IContainer IContext_Providers => this.Providers;
	}
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using MicroIoC.Core;
using MicroIoC.Threading;

namespace MicroIoC.Impl {
	public partial class ConcurrentDerivedContext : IDerivedContext {
		private readonly IContext parentContext;
		public IContext ParentContext => this.parentContext;

		private readonly IContext childContext;
		public IContext ChildContext => this.childContext;

		public IContext PeekContext() => this.ChildContext ?? this.ParentContext;
		public IContext Context => this.PeekContext() ?? throw new MissingContextException();
	
		public ConcurrentDerivedContext(IContext parentContext, IContext childContext) {
			if (parentContext is null && childContext is null) {
				throw new ArgumentNullException(null, $"One of arguments ('{parentContext}', '{childContext}') must be set.");
			}

			this.parentContext = parentContext;
			this.childContext = childContext;
		}
	}

	partial class ConcurrentDerivedContext : ConcurrentContextBase {
		private readonly SyncRoot syncRoot = new SyncRoot();

		private IContainer factories;
		public IContainer Factories => this.syncRoot.DoubleCheckedNotNull(ref factories, this.CreateFactories);
		private IContainer CreateFactories() => new ConcurrentDerivedContainer(this.ParentContext?.Factories, this.ChildContext?.Factories);
		protected override IContainer IContext_Factories => this.Factories;

		private IContainer providers;
		public IContainer Providers => this.syncRoot.DoubleCheckedNotNull(ref providers, this.CreateProviders);
		private IContainer CreateProviders() => new ConcurrentDerivedContainer(this.ParentContext?.Providers, this.ChildContext?.Factories);
		protected override IContainer IContext_Providers => this.Providers;
	}
}

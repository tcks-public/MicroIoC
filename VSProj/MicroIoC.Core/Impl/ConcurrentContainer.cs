﻿using System;
using System.Linq;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;
using MicroIoC.Core;

namespace MicroIoC.Impl {
	public sealed partial class ConcurrentContainer {
		private readonly ConcurrentDictionary<Type, Delegate> items = new ConcurrentDictionary<Type, Delegate>();
	}

	partial class ConcurrentContainer : IContainer {
		public Func<object> Peek(Type type) => this.TypedPeekPrivate<object>(type);
		public Func<T> Peek<T>() => this.TypedPeekPrivate<T>(typeof(T));

		public object Get(Type type) {
			var func = this.Peek(type);
			if (func is null) {
				throw new MissingContainerItemException(type);
			}

			var result = func();
			return result;
		}
		public T Get<T>() {
			var func = this.Peek<T>();
			if (func is null) {
				throw new MissingContainerItemException(typeof(T));
			}

			var result = func();
			return result;
		}

		private Func<T> TypedPeekPrivate<T>(Type type) {
			var found = this.PeekPrivate(type);
			if (found is null) { return null; }

			var result = found as Func<T>;
			if (result is null) {
				result = () => (T)found.DynamicInvoke();
			}

			return result;
		}

		private Delegate PeekPrivate(Type type) {
			this.items.TryGetValue(type, out var result);
			return result;
		}

		public void CopyTo(IDictionary<Type, Delegate> target) {
			foreach (var kp in this.items) {
				target[kp.Key] = kp.Value;
			}
		}
	}
}

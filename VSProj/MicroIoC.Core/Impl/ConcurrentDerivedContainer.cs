﻿using System;
using System.Collections.Generic;
using System.Text;
using MicroIoC.Core;

namespace MicroIoC.Impl {
	public partial class ConcurrentDerivedContainer : IContainer {
		private readonly IContainer parentContainer;
		public IContainer ParentContainer => this.parentContainer;

		private readonly IContainer childContainer;
		public IContainer ChildContainer => this.childContainer;

		public IContainer PeekContainer() => this.ChildContainer ?? this.ParentContainer;
		public IContainer GetContainer() => this.PeekContainer() ?? throw new MissingContainerException();
	
		public ConcurrentDerivedContainer(IContainer parentContainer, IContainer childContainer) {
			if (parentContainer is null && childContainer is null) {
				throw new ArgumentNullException(null, $"One of arguments ('{parentContainer}', '{childContainer}') must be set.");
			}

			this.parentContainer = parentContainer;
			this.childContainer = childContainer;
		}

		public Func<object> Peek(Type type) => this.GetContainer().Peek(type);
		public Func<T> Peek<T>() => this.GetContainer().Peek<T>();
		public object Get(Type type) => this.GetContainer().Get(type);
		public T Get<T>() => this.GetContainer().Get<T>();
		public void CopyTo(IDictionary<Type, Delegate> target) {
			this.parentContainer?.CopyTo(target);
			this.childContainer?.CopyTo(target);
		}
	}
}

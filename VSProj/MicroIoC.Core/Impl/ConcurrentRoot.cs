﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using MicroIoC.Core;

namespace MicroIoC.Impl {
	public sealed class ConcurrentRoot : IRoot {
		private readonly object syncRoot = new object();

		private volatile IContext mainContext;
		public IContext MainContext {
			get {
				var result = this.mainContext;
				if (result is null) {
					lock (syncRoot) {
						result = this.mainContext;
						if (result is null) {
							this.mainContext = result = this.CreateContext();
						}
					}
				}

				return result;
			}
		}

		private readonly AsyncLocal<IContext> localContext = new AsyncLocal<IContext>();
		public IContext LocalContext => this.localContext.Value;

		public IContext CreateContext() => new ConcurrentContext();
		public IDerivedContext CreateDerivedContext(IContext parentContext, IContext childContext) => new ConcurrentDerivedContext(parentContext, childContext);

		public void ResetMainContext() {
			this.mainContext = null;
		}
		public void ResetMainContext(IContext context) {
			this.mainContext = context;
		}

		public void RunInContext(IContext context, Action contextualAction) {
			if (context is null) { throw new ArgumentNullException(nameof(context)); }
			if (contextualAction is null) { return; }

			this.RunInContextPrivate(context, contextualAction);
		}
		private void RunInContextPrivate(IContext context, Action contextualAction) {
			var prevContext = this.localContext.Value;
			try {
				this.localContext.Value = context;
				contextualAction();
			}
			finally {
				this.localContext.Value = prevContext;
			}
		}

		public void RunInDerivedContext(Action contextualAction) {
			if (contextualAction is null) { return; }

			var childContext = this.CreateContext();
			this.RunInDerivedContextPrivate(childContext, contextualAction);
		}
		public void RunInDerivedContext(IContext childContext, Action contextualAction) {
			if (childContext is null) { throw new ArgumentNullException(nameof(childContext)); }
			if (contextualAction is null) { return; }

			this.RunInDerivedContextPrivate(childContext, contextualAction);
		}
		private void RunInDerivedContextPrivate(IContext childContext, Action contextualAction) {
			var parentContext = this.LocalContext ?? this.MainContext;
			var derivedContext = this.CreateDerivedContext(parentContext, childContext);
			this.RunInContext(derivedContext, contextualAction);
		}
	}
}

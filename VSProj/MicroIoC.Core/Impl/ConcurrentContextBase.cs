﻿using System;
using System.Collections.Generic;
using System.Text;
using MicroIoC.Core;

namespace MicroIoC.Impl {
	public abstract class ConcurrentContextBase : IContext {
		protected abstract IContainer IContext_Factories { get; }
		IContainer IContext.Factories => this.IContext_Factories;

		protected abstract IContainer IContext_Providers { get; }
		IContainer IContext.Providers => this.IContext_Providers;

		public virtual IContext CreateMainContext() => new ConcurrentContext();
		public virtual IContext CreateDerivedContext(IContext childContext) => new ConcurrentDerivedContext(this, childContext);
	}
}

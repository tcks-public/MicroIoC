﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MicroIoC.Core {
	public interface IDerivedContext : IContext {
		IContext ParentContext { get; }
		IContext ChildContext { get; }
	}
}

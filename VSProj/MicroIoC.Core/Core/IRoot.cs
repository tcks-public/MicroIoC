﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MicroIoC.Core {
	public interface IRoot {
		/// <summary>
		/// Main context of this root.
		/// </summary>
		IContext MainContext { get; }

		/// <summary>
		/// Local context of current execution context.
		/// </summary>
		IContext LocalContext { get; }

		/// <summary>
		/// Creates new instance of <see cref="IContext"/>.
		/// </summary>
		/// <returns></returns>
		IContext CreateContext();

		/// <summary>
		/// Creates new instance of <see cref="IDerivedContext"/>.
		/// </summary>
		/// <param name="parentContext"></param>
		/// <param name="childContext"></param>
		/// <returns></returns>
		IDerivedContext CreateDerivedContext(IContext parentContext, IContext childContext);

		/// <summary>
		/// Resets <see cref="MainContext"/> to default value.
		/// </summary>
		void ResetMainContext();

		/// <summary>
		/// Resets <see cref="MainContext"/> to <paramref name="context"/>.
		/// </summary>
		/// <param name="context">Can not be null.</param>
		void ResetMainContext(IContext context);

		/// <summary>
		/// Runs <paramref name="contextualAction"/> with <paramref name="context"/> as <see cref="LocalContext"/>.
		/// After exit, the <see cref="LocalContext"/> will be same as in enter.
		/// </summary>
		/// <param name="context">Nullable context.</param>
		/// <param name="contextualAction">Nullable action.</param>
		void RunInContext(IContext context, Action contextualAction);

		void RunInDerivedContext(Action contextualAction);

		void RunInDerivedContext(IContext childContext, Action contextualAction);
	}
}

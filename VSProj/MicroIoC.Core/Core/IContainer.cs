﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MicroIoC.Core {
	public interface IContainer {
		Func<object> Peek(Type type);
		Func<T> Peek<T>();

		object Get(Type type);
		T Get<T>();

		void CopyTo(IDictionary<Type, Delegate> target);
	}
}

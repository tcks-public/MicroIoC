﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MicroIoC.Core {
	public interface IContext {
		IContainer Factories { get; }
		IContainer Providers { get; }

		IContext CreateMainContext();
		IContext CreateDerivedContext(IContext childContext);
	}
}

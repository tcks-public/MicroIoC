﻿using System;
using System.Collections.Generic;
using System.Text;
using MicroIoC.Core;
using MicroIoC.Impl;

namespace MicroIoC {
	/// <summary>
	/// Provides functionality of IoC with simple API.
	/// </summary>
	public static class IoC {
		private static readonly object syncRoot = new object();

		#region Root
		private static volatile IRoot root;
		public static IRoot Root {
			get {
				var result = root;
				if (result is null) {
					lock (syncRoot) {
						result = root;
						if (result is null) {
							root = result = new ConcurrentRoot();
						}
					}
				}

				return result;
			}
			set {
				lock (syncRoot) {
					root = value;
				}
			}
		}
		#endregion Root

		public static IContext PeekContext() {
			var r = Root;
			return r.LocalContext ?? r.MainContext;
		}
		public static IContext GetContext() => PeekContext() ?? throw new MissingContextException();

		#region Factories
		public static T Create<T>() => GetContext().Factories.Get<T>();
		public static object Create(Type type) => GetContext().Factories.Get(type);

		public static Func<T> PeekFactory<T>() => GetContext().Factories.Peek<T>();
		public static Func<object> PeekFactory(Type type) => GetContext().Factories.Peek(type);
		#endregion Factories

		#region Providers
		public static T Get<T>() => GetContext().Providers.Get<T>();
		public static object Get(Type type) => GetContext().Providers.Get(type);

		public static Func<T> PeekProvider<T>() => GetContext().Providers.Peek<T>();
		public static Func<object> PeekProvider(Type type) => GetContext().Providers.Peek(type);
		#endregion Providers
	}
}

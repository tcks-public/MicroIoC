﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace MicroIoC {
	public class MissingContainerItemException : Exception {
		public object Key { get; }

		public MissingContainerItemException() : base("The item from IoC container is missing.") { }
		public MissingContainerItemException(object key) : base($"The item (key: '{key}') from IoC container is missing.") {
			this.Key = key;
		}

		public MissingContainerItemException(string message) : base(message) { }
		public MissingContainerItemException(object key, string message) : base(message) {
			this.Key = key;
		}

		public MissingContainerItemException(string message, Exception innerException) : base(message, innerException) { }
		public MissingContainerItemException(object key, string message, Exception innerException) : base(message, innerException) {
			this.Key = key;
		}

		protected MissingContainerItemException(SerializationInfo info, StreamingContext context) : base(info, context) { }
	}
}

﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace MicroIoC {
	public class MissingContainerException : Exception {
		public MissingContainerException() : base("IoC container is missing") { }

		public MissingContainerException(string message) : base(message) { }

		public MissingContainerException(string message, Exception innerException) : base(message, innerException) { }

		protected MissingContainerException(SerializationInfo info, StreamingContext context) : base(info, context) { }
	}
}
